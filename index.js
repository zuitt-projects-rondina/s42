/*console.log("hello")*/

//we are getting the document in the html element with the id txt-first-name

console.log(document);
const txtFirstName = document.querySelector("#txt-first-name")

//alternative way of targeting an element
		//document.getElementById("txt-first-name")
		// document.getElementByClassName()
		// document.getElementByTagName()

const spanFullName = document.querySelector("#span-full-name")

txtFirstName.addEventListener('keyup', (event) => {

		spanFullName.innerHTML = txtFirstName.value
})


txtFirstName.addEventListener('keyup', (event) => {

		console.log(event.target);
		console.log(event.target.value);

})

