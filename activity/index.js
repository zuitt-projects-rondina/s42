

const txtFirstName = document.querySelector("#txt-first-name")


const txtLastName = document.querySelector("#txt-last-name")


const spanFullName = document.querySelector("#span-full-name")


fullName = () => {

	spanFullName.innerHTML = txtFirstName.value + " " +txtLastName.value;

};

addEventListener('keyup',fullName);




// alternative solution

/*

const updateFullName = () => {
	let firstName = txtFirstName.value
	let lastName = txtLastName.value

	spanFullName = `${firstName} ${lastName}`;

}


txtLastName.addEventListener('keyup', updateFullName);
txtFirstName.addEventListener('keyup', updateFullName);


*/